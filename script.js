function updateActiveInfo(update){
    const ele = document.getElementById('active-tasks-number');
    let value = Number(ele.innerText);
    update ? value++ : value--;
    ele.innerText = value;
}

function addTaskCompletedEventListener(ele){
    ele.addEventListener('click', e => {
        const parent = e.target.parentNode;
        if(parent.classList.contains('active')){
            parent.classList.remove('active');
            parent.classList.add('completed');
            
            e.target.classList.add('check-img-completed');
            e.target.nextElementSibling.classList.add('comleted-task-detail');
            e.target.setAttribute('src', 'images/icon-check-black.svg');
            updateActiveInfo(false);
        }
        else{
            parent.classList.add('active');
            parent.classList.remove('completed');
            
            e.target.classList.remove('check-img-completed');
            e.target.nextElementSibling.classList.remove('comleted-task-detail');
            e.target.setAttribute('src', 'images/icon-check.svg');
            updateActiveInfo(true);
        }
        
    });
}

function addRemovetaskEventListener(ele){
    ele.addEventListener('click', e => {
        if(e.target.parentNode.classList.contains('active')){
            updateActiveInfo(false);
        }
        e.target.parentNode.remove();
    });
}

function createTaskElement(text){
    const newTask = document.createElement('div');
    newTask.className = 'task task-block active';

    const checkImg = document.createElement('img');
    checkImg.setAttribute('src', 'images/icon-check.svg');
    checkImg.setAttribute('alt', 'Completed');
    checkImg.className = 'check-img'
    addTaskCompletedEventListener(checkImg);

    const closeImg = document.createElement('img');
    closeImg.setAttribute('src', 'images/icon-cross.svg');
    closeImg.setAttribute('alt', 'remove');
    closeImg.className = 'close-img';
    addRemovetaskEventListener(closeImg);

    const spanElement = document.createElement('span');
    spanElement.className = 'task-detail';
    spanElement.appendChild(document.createTextNode(text));

    newTask.appendChild(checkImg);
    newTask.appendChild(spanElement);
    newTask.appendChild(closeImg);
    
    newTask.addEventListener('mouseover',e => {
        newTask.getElementsByClassName('close-img')[0].style.display = 'inline-block';
    });
    newTask.addEventListener('mouseleave',e => {
        newTask.getElementsByClassName('close-img')[0].style.display = 'none';
    });
   
    return newTask; 
}


const addTask = document.getElementById('task-input');
addTask.addEventListener('keyup', e => {
    if(e.keyCode === 13 && e.target.value.trim() !== ''){
        const newTaskElement = createTaskElement(e.target.value);
        document.getElementById('task-list').appendChild(newTaskElement);
        updateActiveInfo(true);
        e.target.value = '';
    }
});

const clearCompleted = document.getElementById('remove-completed');
clearCompleted.addEventListener('click', e => {
    const tasks = document.getElementsByClassName('task-block');
    Array.from(tasks).forEach(ele => {
        if(ele.classList.contains('completed')){
            ele.remove();
        }
    });
});

const showCompleted = document.getElementById('filter-completed');
showCompleted.addEventListener('click', e => {
    const tasks = document.getElementsByClassName('task');
    Array.from(tasks).forEach(ele => {
        if(ele.classList.contains('completed')){
            ele.style.display = 'flex';
        }
        else {
            ele.style.display = 'none';
        }
    });
    Array.from(showCompleted.parentNode.children).forEach(ele => ele.classList.remove('filter-element-active'));
    showCompleted.classList.add('filter-element-active');
});

const showActive = document.getElementById('filter-active');
showActive.addEventListener('click', e => {
    const tasks = document.getElementsByClassName('task');
    Array.from(tasks).forEach(ele => {
        if(ele.classList.contains('active')){
            ele.style.display = 'flex';
        }
        else {
            ele.style.display = 'none';
        }
    });
    Array.from(showActive.parentNode.children).forEach(ele => ele.classList.remove('filter-element-active'));
    showActive.classList.add('filter-element-active');
});

const showAll = document.getElementById('filter-all');
showAll.addEventListener('click', e => {
    const tasks = document.getElementsByClassName('task');
    Array.from(tasks).forEach(ele => ele.style.display = 'flex');
    Array.from(showAll.parentNode.children).forEach(ele => ele.classList.remove('filter-element-active'));
    showAll.classList.add('filter-element-active');
});
